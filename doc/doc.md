# Flex Layout

## 教程文档

- 菜鸟教程：http://www.runoob.com/w3cnote/flex-grammar.html
- A Complete Guide to Flexbox https://css-tricks.com/snippets/css/a-guide-to-flexbox/
- Flex 布局示例(网友JailBreak参照阮一峰老师的文章实现) http://static.vgee.cn/static/index.html
- JailBreak的博客 http://www.vgee.cn/
- flex案例 http://www.lixuejiang.me/2016/10/23/flex%E6%A1%88%E4%BE%8B/
- 一个完整的Flexbox指南  https://www.w3cplus.com/css3/a-guide-to-flexbox.html
- “老”的Flexbox和“新”的Flexbox https://www.w3cplus.com/css3/old-flexbox-and-new-flexbox.html
- 使用Flexbox：新旧语法混用实现最佳浏览器兼容  https://www.w3cplus.com/css3/using-flexbox.html
- flexbox 相关的文章 https://www.w3cplus.com/blog/tags/157.html
- Flex 布局教程：语法篇 http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html
- Flex 布局教程：实例篇 http://www.ruanyifeng.com/blog/2015/07/flex-examples.html
- A Visual Guide to CSS3 Flexbox Properties https://scotch.io/tutorials/a-visual-guide-to-css3-flexbox-properties

**注意，设为 Flex 布局以后，子元素的float、clear和vertical-align属性将失效**。

