# icedog.flex-layout

## 简介

flex 布局学习和演示

## 布局示例

### 布局1

[demo 链接](./src/layout-1.html),菜单栏和内容区动态变化宽度

### 布局2

[demo 链接](./src/layout-2.html),菜单栏和内容区动态变化宽度,菜单在左侧

### 布局3

[demo 链接](./src/layout-3.html),菜单栏固定,内容区动态变化宽度

### 布局4

[demo 链接](./src/layout-4.html),三列都是动态的

### 布局5

[demo 链接](./src/layout-5.html),四列都是动态变化的

### 布局6

[demo 链接](./src/layout-6.html),浮动菜单

### 布局7

[demo 链接](./src/layout-7.html),头部和内容区动态变化，两侧菜单固定

### 布局8

[demo 链接](./src/layout-8.html),头部、内容区总是显示在中间，宽度设置为960px

### 布局9

[demo 链接](./src/layout-9.html),头部、底部和内容区动态变化，两侧菜单固定

### 布局10

[demo 链接](./src/layout-10.html),骰子图案1-9

### 布局11

[demo 链接](./src/layout-11.html),grid 布局

### 布局12

[demo 链接](./src/layout-12.html),圣杯布局

### 布局13

[demo 链接](./src/layout-13.html)，

Flexbox 布局的最简单表单 - 阮一峰的网络日志
http://www.ruanyifeng.com/blog/2018/10/flexbox-form.html

Flex 布局教程：语法篇 - 阮一峰的网络日志
https://www.ruanyifeng.com/blog/2015/07/flex-grammar.html

Flex 布局教程：语法篇 - 阮一峰的网络日志
https://www.ruanyifeng.com/blog/2015/07/flex-grammar.html

